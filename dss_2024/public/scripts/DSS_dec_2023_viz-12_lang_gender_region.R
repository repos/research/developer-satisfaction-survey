# Load Packages-----------------------------------------------------------------------------

# Use pacman package to load the following packages (and install any that are not installed)
pacman::p_load(showtext, tidyverse, kableExtra, pollster, here, dplyr, likert, ggh4x, 
               viridis, svglite, eulerr, circlize, plyr)  # plyr is needed for likert bug
font_add_google("Montserrat", "Montserrat")

# Load Data -------------------------------------------------------------------------------

# Read in cleaned dataset (created via "DSS_2023_Data_Cleaning... .R" file)
df_import<- read_csv(here::here("outputs/DSS_december_clean.csv")) %>%
  filter(completion_tri=="Complete") %>%
  filter(is_spam==FALSE) %>%
  filter(is_test==FALSE)
df <- df_import

# Functions ---------------------------------------------------------------------

## ggplot themes ---------

theme_dss_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_minimal() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", hjust=0), 
          plot.caption = element_text(size=10, hjust=1, margin=margin(15,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          axis.title = element_blank(), 
          axis.ticks.y = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          text = element_text( family="Montserrat", size=6) )
}

theme_dss_facet_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_bw() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", margin = margin(0,0,10,0), hjust=0), 
          plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(),  panel.grid.minor = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(), 
          axis.title.y = element_blank() ,
          axis.text.x =  element_text(size=16),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          strip.text = element_text(size = 20),
          text = element_text( family="Montserrat") )
}

theme_likert_png <- function(){
  
  theme(axis.text=element_text(size=22, family = "Montserrat"),
        legend.text = element_text(family="Montserrat", size=20),
        legend.title = element_blank(),
        plot.title = element_text(size=28, face="bold", margin = margin(0,0,30,0)), 
        plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
        text = element_text(family="Montserrat"),
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        panel.grid.minor = element_blank(),
        panel.grid.major = element_blank())
  
}



# GENDER ------------------------------------------------------------------------

## Gender (condensed categories, not mutually exclusive) ----

## Non-mutually exclusive categories: man, woman, transgender, nonbinary, genderqueer, 
## gender_rf (prefer not to say), gender_other_yn (other gender provided yes/no)

# Prep dfs
man_topline <- topline(df, variable=man, weight=weight) %>% mutate(gender_cat="Man")
woman_topline <- topline(df, variable=woman, weight=weight) %>% mutate(gender_cat="Woman")
transgender_topline <- topline(df, variable=transgender, weight=weight) %>% mutate(gender_cat="GenderDiverse")
nonbinary_topline <- topline(df, variable=nonbinary, weight=weight) %>% mutate(gender_cat="GenderDiverse")
genderqueer_topline <- topline(df, variable=genderqueer, weight=weight) %>% mutate(gender_cat="GenderDiverse")
gender_rf_topline <- topline(df, variable=gender_rf, weight=weight) %>% mutate(gender_cat="Prefer not to say")
gender_other_topline <- topline(df, variable=gender_other_yesno, weight=weight) %>% mutate(gender_cat="GenderDiverse")

## Combine all categories into df
gender_cats_topline_combo <- rbind(man_topline, woman_topline, transgender_topline, nonbinary_topline,
                               genderqueer_topline, gender_rf_topline, gender_other_topline)

## Aggregate the Yes to combine the three genderdiverse groups
temp_gender_cats_topline_combo_agg_percent <- gender_cats_topline_combo %>%
  filter(Response=="Yes") %>%
  group_by(gender_cat, Response) %>%
  dplyr::summarise(`Valid Percent` = sum(`Valid Percent`)) %>%
  ungroup()

## Aggregate the Yes to combine the three genderdiverse groups
temp_gender_cats_topline_combo_agg_freq <- gender_cats_topline_combo %>%
  filter(Response=="Yes") %>%
  group_by(gender_cat, Response) %>%
  dplyr::summarise(Frequency = sum(Frequency)) %>%
  ungroup()

## Join
gender_cats_topline_combo <- temp_gender_cats_topline_combo_agg_percent %>%
  full_join(., temp_gender_cats_topline_combo_agg_freq, by=c("gender_cat", "Response"))
gender_cats_topline_combo$gender_cat[gender_cats_topline_combo$gender_cat=="GenderDiverse"] <- "Gender Diverse"

## Save subset for future plotting
write_csv(gender_cats_topline_combo, "subsets/demog_gender_condensed_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_demographics_gender.png",
  width = 1200, height = 600)

plot(
  gender_cats_topline_combo %>%
    filter(Response=="Yes") %>%
    ggplot(aes(x=reorder(gender_cat,`Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ gender identities (Grouped)',
         caption = "December 2023 Developer Satisfaction Survey \nGender groups in this visualization are not mutually exclusive (respondents could select more than one gender answer) \nGender Diverse includes respondents who identified as non-binary, transgender, genderqueer, and/or other gender") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    geom_hline(yintercept = 0, size = 0.10) +
    theme_dss_png()  + 
    scale_x_discrete(labels = function(x) str_wrap(x, width = 20)) +
    scale_y_continuous(limits = c(0, 90), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC", "#3366CC", "#3366CC")) +
    coord_flip()
)

dev.off()


# ENGLISH LANGUAGE -----------------------------------------------------------------

## "Is English your first or primary language?" -----

# Prep 
df$english_primary <- as.character(df$english_primary)
df$english_primary[df$english_primary=="I prefer not to say"] <- "Prefer not to say"
df$english_primary[df$english_primary=="No"] <- "English is not first or primary language"
df$english_primary[df$english_primary=="Yes"] <- "English is first or primary language"

## Save subset for future plotting
write_csv(as.data.frame(topline(df = df , variable = english_primary, weight = weight)), 
          "subsets/demog_first_language_english_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_demographics_English_language_status.png",
  width = 1200, height = 500)

plot(
  topline(df = df , variable = english_primary, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ English language status',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png() +
    scale_fill_manual(values=c("#3366CC", "#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(pde_use) str_wrap(pde_use, width = 18)) +
    scale_y_continuous(labels=function(pct) paste0(pct,"%"),
                       limits = c(0,70), expand = c(0, 0))
) +
  coord_flip()

dev.off()


## English Language x Roles ----

## Prep the top lines for each role

top_WMF_staff <- topline(df = df %>%  filter(WMF_staff=="Yes") , 
                         variable = english_primary, weight = weight) %>%
  mutate(role = "WMF Staff") %>%
  filter(Response != "(Missing)")

top_Aff_staff <- topline(df = df %>%  filter(Aff_staff=="Yes") , 
                         variable = english_primary, weight = weight) %>%
  mutate(role = "Affiliate Staff")%>%
  filter(Response != "(Missing)")

top_Volunteer <- topline(df = df %>%  filter(Volunteer=="Yes") , 
                         variable = english_primary, weight = weight) %>%
  mutate(role = "Volunteer")%>%
  filter(Response != "(Missing)")

top_role_comparison <- rbind(top_WMF_staff, top_Aff_staff, top_Volunteer )


## Save subset for future plotting
write_csv(top_role_comparison, "subsets/demog_first_language_english_x_role_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_demographics_English_language_status_by_role.png",
  width = 1200, height = 600)

plot(
  top_role_comparison %>%
    ggplot(aes(x=reorder(Response,`Valid Percent`), y=`Valid Percent`, fill=role)) +
    ggh4x::facet_wrap2(.~role, ncol = 3) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title =  'Respondents’ English language status, by role',
         caption = "December 2023 Developer Satisfaction Survey \nRroles are not mutually exclusive (respondents could select more than one role)") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_facet_png() +
    scale_fill_manual(values=c("#3366CC", "#3366CC", "#2Ba8Af")) +
                                          scale_x_discrete(labels = function(pde_use) str_wrap(pde_use, width = 19)) +
    scale_y_continuous(labels=function(pct) paste0(pct,"%"),
                       limits = c(0,80), expand = c(0, 0))
) +
  coord_flip()

dev.off()



# PRIMARY LANGUAGE -----------------------------------------------------------------

## "What is your first, or native language, or what language are you most fluent in?" -----

# excluded from public code: aggregation of first languages with low Ns
# df created: "agg"

# Plot 
png(
  filename = "plots/2023-12_DSS_demographics_primary_language.png",
  width = 1200, height = 700)

plot(
  agg %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66, fill="#3366CC") +
    labs(title = 'First or primary language, or language most fluent in, if not English',
         caption = "December 2023 Developer Satisfaction Survey") +
    theme_dss_png() +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat',check_overlap = T) +
    geom_hline(yintercept = 0, linewidth = 0.10) +
    scale_y_continuous(limits = c(0, 35),  expand = c(0, 0),
                       labels=function(x) paste0(x,"%"))  +
    coord_flip()
)

dev.off()


# REGION -----------------------------------------------------------------

## "In what region do you primarily live??" -----

# excluded from public code: aggregation of regions with low Ns
# df created: "agg"

# plot
png(
  filename = "plots/2023-12_DSS_demographics_region.png",
  width = 1200, height = 800)

plot(
  agg %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, Frequency), y=Frequency, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66, fill="#3366CC") +
    labs(title =  'Regions in which respondents primarily live',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = Frequency ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    geom_hline(yintercept = 0, size = 0.10) +
    theme_dss_png()  + 
    scale_x_discrete(labels = function(x) str_wrap(x, width =40)) +
    scale_y_continuous(limits = c(0, 60), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    coord_flip()
  
)

dev.off()

# Load Packages-----------------------------------------------------------------------------

# Use pacman package to load the following packages (and install any that are not installed)
pacman::p_load(showtext, tidyverse, kableExtra, pollster, here, dplyr, likert, ggh4x, 
               viridis, svglite, eulerr, circlize, plyr)  # plyr is needed for likert bug
font_add_google("Montserrat", "Montserrat")

# Load Data -------------------------------------------------------------------------------

# Read in cleaned dataset (created via "DSS_2023_Data_Cleaning... .R" file)
df_import<- read_csv(here::here("outputs/DSS_december_clean.csv")) %>%
  filter(completion_tri=="Complete") %>%
  filter(is_spam==FALSE) %>%
  filter(is_test==FALSE)
df <- df_import

# Functions ---------------------------------------------------------------------

## ggplot themes ---------

theme_dss_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_minimal() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", hjust=0), 
          plot.caption = element_text(size=10, hjust=1, margin=margin(15,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          axis.title = element_blank(), 
          axis.ticks.y = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          text = element_text( family="Montserrat", size=6) )
}

theme_dss_facet_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_bw() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", margin = margin(0,0,10,0), hjust=0), 
          plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(),  panel.grid.minor = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(), 
          axis.title.y = element_blank() ,
          axis.text.x =  element_text(size=16),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          strip.text = element_text(size = 20),
          text = element_text( family="Montserrat") )
}

theme_likert_png <- function(){
  
  theme(axis.text=element_text(size=22, family = "Montserrat"),
        legend.text = element_text(family="Montserrat", size=20),
        legend.title = element_blank(),
        plot.title = element_text(size=28, face="bold", margin = margin(0,0,30,0)), 
        plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
        text = element_text(family="Montserrat"),
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        panel.grid.minor = element_blank(),
        panel.grid.major = element_blank())
  
}



# OVERALL -------------------------------------------------------------------------

## Overall Productive ----
## "Overall, thinking of your role as a member of the Wikimedia Developer Community in the past year, how productive do you feel?"


# Prep factor levels
df$overall_productive <- as.character(df$overall_productive)
df$overall_productive[df$overall_productive=="I'm not sure"] <- "Unsure"
df$overall_productive <- factor(df$overall_productive, levels = c("Unsure",
                                                                  "Not at all productive" ,
                                                                  "Not very productive" ,
                                                                  "Moderately productive",
                                                                  "Very productive"
                                                                  ) )

## Save subset for future plotting
write_csv(topline(df = df, variable = overall_productive, weight = weight), 
          "subsets/overall_productive_barchart.csv")

# Plot
png(
  filename = "plots/2023-12_DSS_overall_productivity.png",
  width = 1200, height = 600)

plot(
  topline(df = df, variable = overall_productive, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    labs(title = 'How productive respondents feel in their role as a member of \nthe Wikimedia developer community in the past year',
         caption = "December 2023 Developer Satisfaction Survey") +
    theme_dss_png() + 
    scale_x_discrete(labels = function(x) str_wrap(x, width = 24)) +
    scale_y_continuous( labels=function(x) paste0(x,"%"),
                        limits = c(0,60), 
                        expand = c(0, 0) ) +
    scale_fill_manual(values=c("#3366cc", "#3366cc", "#3366cc", "#3366cc", "#3366cc")) +
    coord_flip()
)

dev.off()


## Satisfaction (Development Lifecycle) -----
## "Overall, thinking of your role as a member of the Wikimedia Developer Community in the past \nyear, how satisfied are you with the overall Software Development Life Cycle?"

## Prep dummy columns 
dummy_df <- data.frame (
  "Very dissatisfied"  = 0,
  "Somewhat dissatisfied" = 0,
  "Neutral"  = 0,
  "Somewhat satisfied" = 0,
  "Very satisfied"  = 0,
  "I'm not sure" = 0     
)
dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I'm", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="overall_satisfaction", value="Valid Percent") %>% select(overall_satisfaction)


# Filter out folks who haven't used the overall cluster
df_overall <- df %>% filter(overall_satisfaction!="I have not deployed software to Wikimedia's production overall infrastructure in the past year")

## Crosstabs
overall_sat <- topline(df = df_overall, variable=overall_satisfaction, weight = weight) %>%
  dplyr::rename(overall_satisfaction = Response)

# create Ns
overall_satns <- overall_sat %>%
  filter(overall_satisfaction!="(Missing)") %>%
  mutate(dummy="overall") %>%
  group_by(dummy) %>%
  mutate(n=sum(Frequency)) %>%
  ungroup() %>%
  select(dummy,n) %>%
  distinct(.)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
overall_sat <- overall_sat %>% 
  mutate(overall = "overall") %>%
  full_join(., dummy_df, by="overall_satisfaction") %>%
  select(overall, overall_satisfaction, `Valid Percent`) %>% 
  spread(., key=overall_satisfaction, value="Valid Percent") %>% 
  select(overall, "Very satisfied", "Somewhat satisfied", "Neutral", "Somewhat dissatisfied", "Very dissatisfied")
overall_sat <- as.data.frame(overall_sat)
overall_sat[is.na(overall_sat)] <- 0

## Save subset for future plotting
write_csv(overall_sat, "subsets/overall_satisfaction_likert.csv")


### Likert prep ----

# does work: insert any character (which throws a warning only)
ls_overall_sat <- likert(items = "blank", summary= overall_sat)

## different issue: wrong name of "Items" column in likert.R
names(ls_overall_sat[["results"]])[1]
#> [1] "overall"

# correct column name to "Item"
names(ls_overall_sat[["results"]])[1] <- "Item"


### Likert plot ----
png(
  filename = "plots/2023-12_DSS_overall_software_dev_life_cycle_satisfaction.png",
  width = 1200, height = 500)

plot(
  plot(ls_overall_sat, plot.percents=TRUE, 
       ordered=F, text.size=5, text.color="white", wrap=10) +
    labs(title = 'Respondents’ satisfaction with the overall software development life cycle',
         caption = 'December 2023 Developer Satisfaction Survey \nPercentages do not add up to 100% as "I’m not sure" responses are excluded from the visualization') +
    theme_likert_png() +
    ylim(-80,80) +
    scale_fill_manual(values = c("#002165", "#5c9cf3", "#eaecf0", "#e679ac", "#9e0072"),
                      breaks = c("Very satisfied", "Somewhat satisfied", "Neutral", "Somewhat dissatisfied", "Very dissatisfied")) +
    geom_text(aes(label= paste(round(overall_sat$Neutral,0), "%", sep = "")), y=0, color="dimgrey", size=5) +
    geom_text(aes(label= paste("N=", overall_satns$n[[1]], sep = ""), y=80, x=overall_satns$dummy[[1]]), color="dimgrey", size=4, check_overlap = T) 
  
)

dev.off()


## Overall Developer Community ----

## ""Overall, how much do you agree or disagree with the following statement: 
### the Wikimedia Developer Community is . . . "

## Prep dummy columns 
dummy_df <- data.frame (
  "Strongly agree"  = 0,
  "Somewhat agree" = 0,
  "Neutral"  = 0,
  "Somwhat disagree" = 0,
  "Strongly agree"  = 0,
  "I'm not sure" = 0     
)
dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I'm", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="overall_agreement", value="Valid Percent") %>% select(overall_agreement)

## Shorten neutral
df$overall_welcoming[df$overall_welcoming=="Neither agree nor disagree (neutral)"] <- "Neutral"
df$overall_collaborative[df$overall_collaborative=="Neither agree nor disagree (neutral)"] <- "Neutral"
df$overall_dedicated[df$overall_dedicated=="Neither agree nor disagree (neutral)"] <- "Neutral"
df$overall_diverse[df$overall_diverse=="Neither agree nor disagree (neutral)"] <- "Neutral"

## Crosstabs
overall_sat_welcoming <- topline(df = df, variable=overall_welcoming, weight = weight) %>%
  dplyr::rename(overall_agreement = Response) %>%
  mutate(adjective = '"The community is welcoming"')
overall_sat_collaborative <- topline(df = df, variable=overall_collaborative, weight = weight) %>%
  dplyr::rename(overall_agreement = Response) %>%
  mutate(adjective = '"The community is collaborative"')
overall_sat_dedicated <- topline(df = df, variable=overall_dedicated, weight = weight) %>%
  dplyr::rename(overall_agreement = Response) %>%
  mutate(adjective = '"The community is dedicated"')
overall_sat_diverse <- topline(df = df, variable=overall_diverse, weight = weight) %>%
  dplyr::rename(overall_agreement = Response) %>%
  mutate(adjective = '"The community is diverse"')

combo_overall_sat <- rbind(overall_sat_welcoming, overall_sat_collaborative, 
                           overall_sat_dedicated, overall_sat_diverse)


# create Ns
combo_overall_satns <- combo_overall_sat  %>%
  filter(overall_agreement!="(Missing)") %>%
  group_by(adjective) %>%
  mutate(n=sum(Frequency)) %>%
  ungroup() %>%
  select(adjective,n) %>%
  distinct(.) %>%
  dplyr::rename(Item=adjective)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
combo_overall_sat <- combo_overall_sat %>% 
  full_join(., dummy_df, by="overall_agreement") %>%
  select(adjective, overall_agreement, `Valid Percent`) %>% 
  spread(., key=overall_agreement, value="Valid Percent") %>% 
  select(adjective, "Strongly agree", "Somewhat agree", "Neutral", "Somewhat disagree", "Strongly disagree")
combo_overall_sat <- as.data.frame(combo_overall_sat) %>% filter(!is.na(adjective))
combo_overall_sat [is.na(combo_overall_sat)] <- 0

combo_overall_sat$adjective <- factor(combo_overall_sat$adjective, levels = c(
  '"The community is collaborative"',
  '"The community is dedicated"',
  '"The community is diverse"',
  '"The community is welcoming"'))

## Save subset for future plotting
write_csv(overall_sat, "subsets/overall_adjectives_likert.csv")


### Likert prep ----

# does work: insert any character (which throws a warning only)
ls_combo_overall_sat <- likert(items = "blank", summary= combo_overall_sat)

## different issue: wrong name of "Items" column in likert.R
names(ls_combo_overall_sat[["results"]])[1]
#> [1] "adjective"

# correct column name to "Item"
names(ls_combo_overall_sat[["results"]])[1] <- "Item"


### Likert plot ----
png(
  filename = "plots/2023-12_DSS_overall_community_perception.png",
  width = 1200, height = 700)

plot(
  plot(ls_combo_overall_sat, plot.percents=TRUE, grouping = ls_combo_overall_sat$Item,
       ordered=F, text.size=5, text.color="white", wrap=18) +
    labs(title = 'Respondents’ views of the Wikimedia developer community',
         caption = 'December 2023 Developer Satisfaction Survey \nPercentages do not add up to 100% as "I’m not sure" responses are excluded from the visualization') +
    theme_likert_png() +
    ylim(-90,80) +
    scale_fill_manual(values = c("#002165", "#5c9cf3", "#eaecf0", "#e679ac", "#9e0072"),
                      breaks = c(  "Strongly agree", "Somewhat agree", "Neutral", "Somewhat disagree", "Strongly disagree")) +
    theme(axis.text.y = element_text()) +
    geom_text(aes(label= paste(round(combo_overall_sat$Neutral,0)[[1]], "%", sep = ""), y=0, x=combo_overall_sat$adjective[[1]]), color="dimgrey", size=5) +
    geom_text(aes(label= paste(round(combo_overall_sat$Neutral,0)[[2]], "%", sep = ""), y=0, x=combo_overall_sat$adjective[[2]]), color="dimgrey", size=5) +
    geom_text(aes(label= paste(round(combo_overall_sat$Neutral,0)[[3]], "%", sep = ""), y=0, x=combo_overall_sat$adjective[[3]]), color="dimgrey", size=5) +
    geom_text(aes(label= paste(round(combo_overall_sat$Neutral,0)[[4]], "%", sep = ""), y=0, x=combo_overall_sat$adjective[[4]]), color="dimgrey", size=5) +
        geom_text(aes(label= paste("N=", combo_overall_satns$n[[1]], sep = ""), y=80, x=combo_overall_satns$Item[[1]]), color="dimgrey", size=4, check_overlap = T)   +
        geom_text(aes(label= paste("N=", combo_overall_satns$n[[2]], sep = ""), y=80, x=combo_overall_satns$Item[[2]]), color="dimgrey", size=4, check_overlap = T)   +
        geom_text(aes(label= paste("N=", combo_overall_satns$n[[3]], sep = ""), y=80, x=combo_overall_satns$Item[[3]]), color="dimgrey", size=4, check_overlap = T)   +
        geom_text(aes(label= paste("N=", combo_overall_satns$n[[4]], sep = ""), y=80, x=combo_overall_satns$Item[[4]]), color="dimgrey", size=4, check_overlap = T)   
)

dev.off()


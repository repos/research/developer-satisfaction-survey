# Load Packages-----------------------------------------------------------------------------

# Use pacman package to load the following packages (and install any that are not installed)
pacman::p_load(showtext, tidyverse, kableExtra, pollster, here, dplyr, likert, ggh4x, 
               viridis, svglite, eulerr, circlize, plyr)  # plyr is needed for likert bug
font_add_google("Montserrat", "Montserrat")
here::here()

# Load Data -------------------------------------------------------------------------------

# Read in cleaned dataset (created via "DSS_2023_Data_Cleaning... .R" file)
df_import<- read_csv(here::here("outputs/DSS_december_clean.csv")) %>%
  filter(completion_tri=="Complete") %>%
  filter(is_spam==FALSE) %>%
  filter(is_test==FALSE)
df <- df_import

# Functions ---------------------------------------------------------------------

## ggplot themes ---------

theme_dss_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_minimal() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", hjust=0), 
          plot.caption = element_text(size=10, hjust=1, margin=margin(15,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          axis.title = element_blank(), 
          axis.ticks.y = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          text = element_text( family="Montserrat", size=6) )
}

theme_dss_facet_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_bw() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", margin = margin(0,0,10,0), hjust=0), 
          plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(),  panel.grid.minor = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(), 
          axis.title.y = element_blank() ,
          axis.text.x =  element_text(size=16),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          strip.text = element_text(size = 20),
          text = element_text( family="Montserrat") )
}

theme_likert_png <- function(){
  
  theme(axis.text=element_text(size=22, family = "Montserrat"),
        legend.text = element_text(family="Montserrat", size=20),
        legend.title = element_blank(),
        plot.title = element_text(size=28, face="bold", margin = margin(0,0,30,0)), 
        plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
        text = element_text(family="Montserrat"),
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        panel.grid.minor = element_blank(),
        panel.grid.major = element_blank())
  
}


# CODE QUALITY ---------------------------------------------------------------------------

## What is your perception of the current level of technical debt? ----

## Prep dummy columns 
dummy_df <- data.frame (
  "Very low"  = 0,
  "Somewhat low" = 0,
  "Neither low nor high"  = 0,
  "Somewhat high" = 0,
  "Very high"  = 0,
  "I'm not sure" = 0     
)
dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I’m", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="quality_debt_perception", value="Valid Percent") %>% select(quality_debt_perception)


## Crosstabs
quality_debt_perception <- topline(df = df, variable=quality_debt_perception, weight = weight) %>%
  dplyr::rename(quality_debt_perception = Response)

# create Ns
quality_debt_perceptionns <- quality_debt_perception %>%
  filter(quality_debt_perception!="(Missing)") %>%
  mutate(dummy="debt_perc") %>%
  group_by(dummy) %>%
  mutate(n=sum(Frequency)) %>%
  ungroup() %>%
  select(dummy,n) %>%
  distinct(.)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
quality_debt_perception <- quality_debt_perception %>% 
  mutate(debt_perc = "debt_perc") %>%
  full_join(., dummy_df, by="quality_debt_perception") %>%
  select(debt_perc, quality_debt_perception, `Valid Percent`) %>% 
  spread(., key=quality_debt_perception, value="Valid Percent") %>% 
  select(debt_perc,  "Very low", "Somewhat low", "Neither low nor high", "Somewhat high", "Very high")
quality_debt_perception <- as.data.frame(quality_debt_perception)
quality_debt_perception <- quality_debt_perception %>% filter(!is.na(debt_perc))
quality_debt_perception[is.na(quality_debt_perception)] <- 0

## Save subset for future plotting
write_csv(quality_debt_perception, "subsets/quality_debt_perception_likert.csv")

### Likert prep ----

# does work: insert any character (which throws a warning only)
quality_debt_perception_top  <- likert(items = "blank", summary= quality_debt_perception)

## different issue: wrong name of "Items" column in likert.R
names(quality_debt_perception_top [["results"]])[1]
#> [1] "debt_perc"

# correct column name to "Item"
names(quality_debt_perception_top [["results"]])[1] <- "Item"


### Likert plot ----
png(
  filename = "plots/2023-12_DSS_code_quality_technical_debt_perception.png",
  width = 1200, height = 500)

plot(
  plot(quality_debt_perception_top , plot.percents=TRUE, 
       ordered=F, text.size=5, text.color="white") +
    labs(title = 'Respondents’ perceptions of current level of technical debt',
         caption = 'December 2023 Developer Satisfaction Survey \nPercentages do not add up to 100% as "I’m not sure" responses are excluded from the visualization') +
    theme_likert_png() +
    ylim(-80,80) +
    scale_fill_manual(values = c("#002165", "#5c9cf3", "#eaecf0", "#e679ac", "#9e0072"),
                      breaks = c("Very low", "Somewhat low", "Neither low nor high", "Somewhat high", "Very high")) +
    geom_text(aes(label= paste(round(quality_debt_perception$`Neither low nor high`,0), "%", sep = "")), y=0, color="dimgrey", size=5, check_overlap = T) +
    geom_text(aes(label= paste("N=", quality_debt_perceptionns$n[[1]], sep = ""), y=80, x=quality_debt_perceptionns$dummy[[1]]), color="dimgrey", size=4, check_overlap = T) 
  

)

dev.off()


## How has technical debt affected your productivity? -----

# df
qde_df <- df

# Prep factor levels
qde_df$quality_debt_effects <- as.character(qde_df$quality_debt_effects)
qde_df$quality_debt_effects[qde_df$quality_debt_effects=="Technical debt had no effect on my productivity"] <- "No effect"
qde_df$quality_debt_effects[qde_df$quality_debt_effects=="Technical debt had a minor effect on my productivity"] <- "Minor effect"
qde_df$quality_debt_effects[qde_df$quality_debt_effects=="Technical debt had a major effect on my productivity"] <- "Major effect"
qde_df$quality_debt_effects[qde_df$quality_debt_effects=="I'm not sure"] <- "Unsure"
qde_df$quality_debt_effects <- factor(qde_df$quality_debt_effects, levels = c( "Major effect", "Minor effect", "No effect", "Unsure") )

# Save subset for later plotting
write_csv(qde_df, "subsets/quality_debt_effects_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_code_quality_technical_debt_effects.png",
  width = 1200, height = 600)

plot(
  topline(df = qde_df, variable = quality_debt_effects, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ perceived effects of technical debt on their productivity',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png()  + 
    geom_hline(yintercept = 0, size = 0.10) +
    scale_y_continuous(limits = c(0, 40), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC", "#3366CC","#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(x) str_wrap(x, width = 20)) +
    coord_flip()
)

dev.off()



## Perception of code quality (First-party) --------

# "How would you rate the quality of the code you maintain in your role as a member of the Wikimedia Developer Community?"

## Prep dummy columns 
dummy_df <- data.frame (
  "Very low quality"  = 0,
  "Somewhat low quality" = 0,
  "Neither high nor low"  = 0, #(neutral shortened here and below)
  "Somewhat high quality" = 0,
  "Very high quality"  = 0,
  "I'm not sure" = 0     
)
dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I’m", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="quality_firstparty", value="Valid Percent") %>% select(quality_firstparty)

# Shorten 'neutral'
df$quality_firstparty <- as.character(df$quality_firstparty)
df$quality_firstparty[df$quality_firstparty=="Neither low nor high quality (neutral)"] <- "Neither high nor low"

## Crosstabs
quality_firstparty <- topline(df = df, variable=quality_firstparty, weight = weight) %>%
  dplyr::rename(quality_firstparty = Response)

# create Ns
quality_firstpartyns <- quality_firstparty %>%
  filter(quality_firstparty!="(Missing)") %>%
  mutate(dummy="quality_fp") %>%
  group_by(dummy) %>%
  mutate(n=sum(Frequency)) %>%
  ungroup() %>%
  select(dummy,n) %>%
  distinct(.)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
quality_firstparty <- quality_firstparty %>% 
  mutate(quality_fp = "quality_fp") %>%
  full_join(., dummy_df, by="quality_firstparty") %>%
  select(quality_fp, quality_firstparty, `Valid Percent`) %>% 
  spread(., key=quality_firstparty, value="Valid Percent") %>% 
  select(quality_fp,  "Very high quality", "Somewhat high quality","Neither high nor low",  "Somewhat low quality", "Very low quality" )
quality_firstparty <- as.data.frame(quality_firstparty)
quality_firstparty <- quality_firstparty %>% filter(!is.na(quality_fp))
quality_firstparty[is.na(quality_firstparty)] <- 0

# Save subset for later plotting
write_csv(quality_firstparty, "subsets/quality_firstparty_likert.csv")


### Likert prep ----

# does work: insert any character (which throws a warning only)
quality_firstparty_top  <- likert(items = "blank", summary= quality_firstparty)

## different issue: wrong name of "Items" column in likert.R
names(quality_firstparty_top [["results"]])[1]
#> [1] "quality_fp"

# correct column name to "Item"
names(quality_firstparty_top [["results"]])[1] <- "Item"


### Likert plot ----
png(
  filename = "plots/2023-12_DSS_code_quality_first-party_code.png",
  width = 1200, height = 500)

plot(
  plot(quality_firstparty_top, plot.percents=TRUE, 
       ordered=F, text.size=5, text.color="white") +
    labs(title = 'Respondents’ ratings of the quality of the code they maintain',
         caption = 'December 2023 Developer Satisfaction Survey \nPercentages do not add up to 100% as "I’m not sure" responses are excluded from the visualization.') +
    theme_likert_png() +
    ylim(-80,80) +
    scale_fill_manual(values = c("#002165", "#5c9cf3", "#eaecf0", "#e679ac", "#9e0072"),
                      breaks = c(  "Very high quality", "Somewhat high quality","Neither high nor low",  "Somewhat low quality", "Very low quality")) +
    geom_text(aes(label= paste(round(quality_firstparty$`Neither high nor low`,0), "%", sep = "")), y=0, color="dimgrey", size=5, check_overlap = T) +
    geom_text(aes(label= paste("N=", quality_firstpartyns$n[[1]], sep = ""), y=80, x=quality_firstpartyns$dummy[[1]]), color="dimgrey", size=4, check_overlap = T) 
)

dev.off()


## Perception of code quality (Third-party) --------

# "How would you rate the quality of third-party code (libraries, language ecosystems) 
# that you depend on in your role as a member of the Wikimedia Developer Community?"

## Prep dummy columns 
dummy_df <- data.frame (
  "Very low quality"  = 0,
  "Somewhat low quality" = 0,
  "Neither high nor low"  = 0,
  "Somewhat high quality" = 0,
  "Very high quality"  = 0,
  "I'm not sure" = 0     
)
dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I’m", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="quality_thirdparty", value="Valid Percent") %>% select(quality_thirdparty)

# Shorten 'neutral'
df$quality_thirdparty <- as.character(df$quality_thirdparty)
df$quality_thirdparty[df$quality_thirdparty=="Neither low nor high quality (neutral)"] <- "Neither high nor low"

## Crosstabs
quality_thirdparty <- topline(df = df, variable=quality_thirdparty, weight = weight) %>%
  dplyr::rename(quality_thirdparty = Response)

# create Ns
quality_thirdpartyns <- quality_thirdparty %>%
  filter(quality_thirdparty!="(Missing)") %>%
  mutate(dummy="quality_tp") %>%
  group_by(dummy) %>%
  mutate(n=sum(Frequency)) %>%
  ungroup() %>%
  select(dummy,n) %>%
  distinct(.)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
quality_thirdparty <- quality_thirdparty %>% 
  mutate(quality_tp = "quality_tp") %>%
  full_join(., dummy_df, by="quality_thirdparty") %>%
  select(quality_tp, quality_thirdparty, `Valid Percent`) %>% 
  spread(., key=quality_thirdparty, value="Valid Percent") %>% 
  select(quality_tp,  "Very high quality", "Somewhat high quality","Neither high nor low",  "Somewhat low quality", "Very low quality")
quality_thirdparty <- as.data.frame(quality_thirdparty)
quality_thirdparty <- quality_thirdparty %>% filter(!is.na(quality_tp))
quality_thirdparty[is.na(quality_thirdparty)] <- 0

# Save subset for later plotting
write_csv(quality_thirdparty, "subsets/quality_thirdparty_likert.csv")


### Likert prep ----

# does work: insert any character (which throws a warning only)
quality_thirdparty_top  <- likert(items = "blank", summary= quality_thirdparty)

## different issue: wrong name of "Items" column in likert.R
names(quality_thirdparty_top [["results"]])[1]
#> [1] "quality_tp"

# correct column name to "Item"
names(quality_thirdparty_top [["results"]])[1] <- "Item"


### Likert plot ----
png(
  filename = "plots/2023-12_DSS_code_quality_third-party_code.png",
  width = 1200, height = 500)

plot(
  plot(quality_thirdparty_top, plot.percents=TRUE, 
       ordered=F, text.size=5, text.color="white") +
    labs(title = 'Respondents’ ratings of the quality of third-party code that ythey depend on \n(i.e., libraries, language ecosystems)',
         caption = 'December 2023 Developer Satisfaction Survey \nPercentages do not add up to 100% as "I’m not sure" responses are excluded from the visualization') +
    theme_likert_png() +
    ylim(-80,80) +
    scale_fill_manual(values = c("#002165", "#5c9cf3", "#eaecf0", "#e679ac", "#9e0072"),
                      breaks = c(  "Very high quality", "Somewhat high quality","Neither high nor low",  "Somewhat low quality", "Very low quality")) +
    geom_text(aes(label= paste(round(quality_thirdparty$`Neither high nor low`,0), "%", sep = "")), y=0, color="dimgrey", size=5, check_overlap = T) +
    geom_text(aes(label= paste("N=", quality_thirdpartyns$n[[1]], sep = ""), y=80, x=quality_thirdpartyns$dummy[[1]]), color="dimgrey", size=4, check_overlap = T) 
)

dev.off()


## How has code quality affected your productivity? -----

# "In the past year, how has code quality affected your productivity as  a member of the Wikimedia Developer Community? *"

# df
qpe_df <- df

# Prep factor levels
qpe_df$quality_productivity_effects <- as.character(qpe_df$quality_productivity_effects)
qpe_df$quality_productivity_effects[qpe_df$quality_productivity_effects=="Code quality had no effect on my productivity"] <- "No effect"
qpe_df$quality_productivity_effects[qpe_df$quality_productivity_effects=="Code quality had a minor effect on my productivity"] <- "Minor effect"
qpe_df$quality_productivity_effects[qpe_df$quality_productivity_effects=="Code quality had a major effect on my productivity"] <- "Major effect"
qpe_df$quality_productivity_effects[qpe_df$quality_productivity_effects=="I'm not sure"] <- "Unsure"
qpe_df$quality_productivity_effects <- factor(qpe_df$quality_productivity_effects, levels = c( "Major effect", "Minor effect", "No effect", "Unsure") )

## Save subset for future plotting
write_csv(qpe_df, "subsets/quality_productivity_effects_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_code_quality_effects.png",
  width = 1200, height = 600)

plot(
  topline(df = qpe_df, variable = quality_productivity_effects, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ perceived effects of code quality on their productivity',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png()  + 
    geom_hline(yintercept = 0, size = 0.10) +
    scale_y_continuous(limits = c(0, 50), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC", "#3366CC","#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(x) str_wrap(x, width = 20)) +
    coord_flip()
)

dev.off()


# Load Packages-----------------------------------------------------------------------------

# Use pacman package to load the following packages (and install any that are not installed)
pacman::p_load(showtext, tidyverse, kableExtra, pollster, here, dplyr, likert, ggh4x, 
               viridis, svglite, eulerr, circlize, plyr)  # plyr is needed for likert bug
font_add_google("Montserrat", "Montserrat")

# Load Data -------------------------------------------------------------------------------

# Read in cleaned dataset (created via "DSS_2023_Data_Cleaning... .R" file)
df_import<- read_csv(here::here("outputs/DSS_december_clean.csv")) %>%
  filter(completion_tri=="Complete") %>%
  filter(is_spam==FALSE) %>%
  filter(is_test==FALSE)
df <- df_import

# Functions ---------------------------------------------------------------------

## ggplot themes ---------

theme_dss_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_minimal() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", hjust=0), 
          plot.caption = element_text(size=10, hjust=1, margin=margin(15,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          axis.title = element_blank(), 
          axis.ticks.y = element_blank(),
          axis.text.x = element_blank(),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          text = element_text( family="Montserrat", size=6) )
}

theme_dss_facet_png <- function(){ 
  
  font <- "Montserrat"   #assign font family up front
  
  theme_bw() %+replace%    #replace elements we want to change
    
    theme(plot.title = element_text(size=28, face="bold", margin = margin(0,0,10,0), hjust=0), 
          plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
          legend.position = "none",
          panel.grid.major = element_blank(),  panel.grid.minor = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(), 
          axis.title.y = element_blank() ,
          axis.text.x =  element_text(size=16),
          axis.text.y = element_text(size=20, angle=0, hjust=1),
          strip.text = element_text(size = 20),
          text = element_text( family="Montserrat") )
}

theme_likert_png <- function(){
  
  theme(axis.text=element_text(size=22, family = "Montserrat"),
        legend.text = element_text(family="Montserrat", size=20),
        legend.title = element_blank(),
        plot.title = element_text(size=28, face="bold", margin = margin(0,0,30,0)), 
        plot.caption = element_text(size=10, hjust = 1, margin=margin(30,0,0,0)),
        text = element_text(family="Montserrat"),
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        panel.grid.minor = element_blank(),
        panel.grid.major = element_blank())
  
}


# ORG SUPPORT / OPEN-SOURCE ----------------------------------------------------------

## Wikimedia support of open-source -------

## "Of the following statements, which one comes closest to describing your view of the 
## amount of money, time, and effort Wikimedia spends supporting the open source ecosystem?"

## Prep dummy columns 
dummy_df <- data.frame (
  "It's far too little"  = 0,
  "It's somewhat too little" = 0,
  "It's about right"  = 0,
  "It's somewhat too much" = 0,
  "It's far too much"  = 0,
  "I'm not sure" = 0     
)

dummy_df <- rename_with(dummy_df, ~ gsub(".", " ", .x, fixed = TRUE))
dummy_df <- rename_with(dummy_df, ~ gsub("I m", "I’m", .x, fixed = TRUE))
dummy_df <- dummy_df  %>% gather(., key="org_support", value="Valid Percent") %>% select(org_support)


## Crosstabs
org_support <- topline(df = df %>% filter(is_spam == 0), variable=org_support, weight = weight) %>%
  dplyr::rename(org_support = Response)

# Attach to the dummy_df, to get NAs for zeros.
# Then spread response groups.
org_support <- org_support %>% 
  mutate(org_sup = "org_sup") %>%
  full_join(., dummy_df, by="org_support") %>%
  select(org_sup, org_support, `Valid Percent`) %>% 
  spread(., key=org_support, value="Valid Percent") %>% 
  select(org_sup,  "It's far too much", "It's somewhat too much","It's about right",  "It's somewhat too little", "It's far too little" )
org_support <- as.data.frame(org_support)
org_support <- org_support %>% filter(!is.na(org_sup))
org_support[is.na(org_support)] <- 0

## Save subset for future plotting
write_csv(org_support, "subsets/org_support_likert.csv")


### Plot 
png(
  filename = "plots/2023-12_DSS_organizational_support.png",
  width = 1200, height = 700)

plot(
  topline(df = df %>% filter(is_spam == 0), variable=org_support, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ perceptions of the amount of money, time, and effort \nWikimedia spends supporting the open source ecosystem',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png()  + 
    geom_hline(yintercept = 0, size = 0.10) +
    scale_y_continuous(limits = c(0, 35), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC","#3366CC","#3366CC", "#3366CC","#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(x) str_wrap(x, width = 16)) +
    coord_flip()
)

dev.off()


## Wikimedia as ecosystem/ software upstream --------

## ''"What is your perception of Wikimedia as a free and open source software upstream?"'

# df
orgp_df <- df

# Prep factor levels
orgp_df$org_perception <- factor(orgp_df$org_perception, 
                                 levels = c( "Wikimedia is not a supportive ecosystem", 
                                             "Wikimedia is a somewhat supportive ecosystem", 
                                             "Wikimedia is a very supportive ecosystem", 
                                             "I'm not sure") )

# Save subset for later plotting
write_csv(as.data.frame( topline(df = df, variable = phab_use, weight = weight) ),
          "subsets/org_perception_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_organizational_ecosystem.png",
  width = 1200, height = 500)

plot(
  topline(df = orgp_df %>% filter(is_spam == 0), variable = org_perception, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ perceptions of Wikimedia as a free and \nopen source software upstream',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png()  + 
    geom_hline(yintercept = 0, size = 0.10) +
    scale_y_continuous(limits = c(0, 60), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC", "#3366CC","#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(x) str_wrap(x, width = 26)) +
    coord_flip()
)

dev.off()



## Wikimedia contributions to 3rd-party projects ---------

## "Of the following statements, which one comes closest to describing your
##  view of Wikimedia’s contribution to third-party projects?" 

# df
orgc_df <- df

# Prep factor levels
orgc_df$org_contribution <- factor(orgc_df$org_contribution, 
                                   levels = c( "Wikimedia makes no contribution", 
                                               "Wikimedia makes minor contributions", 
                                               "Wikimedia makes major contributions", 
                                               "I'm not sure") )

# Save subset for later plotting
write_csv(orgc_df, "subsets/org_contribution_barchart.csv")

# Plot 
png(
  filename = "plots/2023-12_DSS_organizational_contribution.png",
  width = 1200, height = 600)

plot(
  topline(df = orgc_df %>% filter(is_spam == 0), variable = org_contribution, weight = weight) %>%
    filter(Response != "(Missing)") %>%
    ggplot(aes(x=reorder(Response, `Valid Percent`), y=`Valid Percent`, fill=Response)) +
    geom_bar(stat = "identity", position = position_dodge(), width = 0.66) +
    labs(title = 'Respondents’ perceptions of Wikimedia’s contribution to \nthird-party projects',
         caption = "December 2023 Developer Satisfaction Survey") +
    geom_text(aes(label = paste0(Frequency), y = `Valid Percent` ), 
              position = position_dodge(width = 0.66), vjust = 0.8, hjust = 1.5,
              color = "white", size = 5,  family = 'Montserrat') +
    theme_dss_png()  + 
    geom_hline(yintercept = 0, size = 0.10) +
    scale_y_continuous(limits = c(0, 50), expand = c(0,0),
                       labels=function(x) paste0(x,"%")) +
    scale_fill_manual(values=c("#3366CC", "#3366CC","#3366CC", "#3366CC")) +
    scale_x_discrete(labels = function(x) str_wrap(x, width = 20)) +
    coord_flip()
  
)

dev.off()

## developer-satisfaction-survey

The purpose of this repo is to publicly document the cleaning, wrangling, and visualization of Wikimedia [Developer Satisfaction Survey](https://www.mediawiki.org/wiki/Developer_Satisfaction_Survey) (DSS) response data. For the 2024 survey, the [Cloud Services annual survey](https://meta.wikimedia.org/wiki/Research:Cloud_Services_Annual_Survey) merged with the DSS.

The .R scripts in this repo are not meant to be run, because the source data is not public. Instead, the purpose of the scripts is public documentation of the cleaning, wrangling, and visualization process.


### Repository structure
```
.
├── dss_2023: Contains files for the 2023 Wikimedia Developer Satisfaction Survey
│   └── public
│       └──scripts: Contains public cleaning and visualization scripts.
├── dss_2024: Contains files for the 2024 Wikimedia Developer Satisfaction Survey
│   └── public
│       └──scripts: Contains public cleaning and visualization scripts.
└── README.md
```


### Authors and acknowledgment
Code was written collaboratively by [@cmyrick](https://gitlab.wikimedia.org/cmyrick), [@hghani](https://gitlab.wikimedia.org/hghani), and [@yumingliou](https://gitlab.wikimedia.org/yumingliou)

### Project status
- [x] 2023 Developer Satisfaction Survey: [results published to MediaWiki](https://www.mediawiki.org/wiki/Developer_Satisfaction_Survey/2023)
- [x] 2024 Developer Satisfaction Survey: [results published to MediaWiki](https://www.mediawiki.org/wiki/Developer_Satisfaction_Survey/2024)
